import { Injectable } from '@angular/core';
import { HttpWrapperService } from './httpWrapper.service';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Transaction } from '../transaction/add-transasction/add-transasction.component';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
    constructor(private http : HttpWrapperService,private router:Router) { }
    
    ngOnInit() {
        
    }

    addTransaction(transaction): Observable<Transaction> {
        console.log(transaction);
        console.log(localStorage.getItem("resource_url")+"transaction");
        return this.http.post<any>(localStorage.getItem("resource_url")+"transaction",transaction).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                console.log("current saving is "+response);
                if(response){
                    this.router.navigateByUrl("dashboard");
                }
                return response;
            }),
            catchError(this.handleError)
        );
    }

    getTopFiveCategoriesBySpend(): Observable<Array<{spend: number, categoryName: string}>> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transactionCategory/topFiveCategoriesBySpend?userId="+localStorage.getItem("user_id")).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                console.log("current saving is "+response);
                return response;
            }),
            catchError(this.handleError)
        );
    }

    private handleError(error: any): Observable<never> {
        return throwError(error || 'Server error');
    }

    getAllCategories(): Observable<Array<{id: number, category: string}>> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transactionCategory").pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                console.log("all categories are "+response);
                return response;
            }),
            catchError(this.handleError)
        );
    }
}