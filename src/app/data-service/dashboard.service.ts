import { Injectable } from '@angular/core';
import { HttpWrapperService } from './httpWrapper.service';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TransactionWithDayName } from '../model/transaction-with-day-name';
import { Transaction } from '../transaction/add-transasction/add-transasction.component';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
    constructor(private http : HttpWrapperService) { }
    
    ngOnInit() {
        
    }

    getTotalSaving(): Observable<string> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transaction/"+"currentSaving?userId="+localStorage.getItem("user_id")).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                console.log("current saving is "+response);
                return response;
            }),
            catchError(this.handleError)
        );
    }

    getTopFiveCategoriesBySpend(): Observable<Array<{spend: number, categoryName: string}>> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transactionCategory/topFiveCategoriesBySpend?userId="+localStorage.getItem("user_id")).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                console.log("current saving is "+response);
                return response;
            }),
            catchError(this.handleError)
        );
    }

    getTransactionWithDayName(): Observable<Map<String,TransactionWithDayName>> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transaction/transactionWithDayName?userId="+localStorage.getItem("user_id")).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                console.log("current response is "+response);
                return response;
            }),
            catchError(this.handleError)
        );
    }

    getCurrentMonthStats(): Observable<Map<String,Map<String,Number>>> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transaction/monthlyStats?userId="+localStorage.getItem("user_id")).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                return response;
            }),
            catchError(this.handleError)
        );
    }

    getTransactions(): Observable<Array<Transaction>> {
        return this.http.get<any>(localStorage.getItem("resource_url")+"transaction?userId="+localStorage.getItem("user_id")).pipe(
            // TO DO GET COMPLETE TASKSUMMARY MODEL
            map(response => {
                return response;
            }),
            catchError(this.handleError)
        );
    }
    

    private handleError(error: any): Observable<never> {
        return throwError(error || 'Server error');
    }
}