import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HttpWrapperService {
    constructor(private http: HttpClient) {}

    get<T>(url: string): Observable<T> {
        return this.http.get<T>(url);
    }

    getBlob(url: string): Observable<Blob> {
        return this.http.get(url, {
            responseType: 'blob'
        });
    }

    post<T>(url: string, body: any): Observable<T> {
        return this.http.post<T>(url, body);
    }

    postBlob(url: string, body: any): Observable<Blob> {
        return this.http.post(url, body, {
            responseType: 'blob'
        });
    }

    postOptions<T>(url: string, body: any, options: any): Observable<HttpEvent<T>> {
        return this.http.post<T>(url, body, options);
    }

    put<T>(url: string, body: any): Observable<T> {
        return this.http.put<T>(url, body);
    }

    delete<T>(url: string): Observable<T> {
        return this.http.delete<T>(url);
    }

    patch<T>(url: string, body: string): Observable<T> {
        return this.http.patch<T>(url, body);
    }
}
