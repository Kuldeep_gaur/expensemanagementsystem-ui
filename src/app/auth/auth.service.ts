import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpHeaders, HttpParams, HttpEvent } from '@angular/common/http';
import { HttpWrapperService } from '../data-service/httpWrapper.service';
import { Observable, throwError, observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as jwt_decod from "jwt-decode";
import { AuthToken } from '../model/AuthToken';

const auth_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiVVNFUl9DTElFTlRfUkVTT1VSQ0UiLCJVU0VSX0FETUlOX1JFU09VUkNFIl0sInVzZXJfbmFtZSI6ImFkbWluIiwic2NvcGUiOlsicm9sZV9hZG1pbiJdLCJpZCI6MSwiZXhwIjoxNTU0NjYwMjA0LCJhdXRob3JpdGllcyI6WyJyb2xlX2FkbWluIiwiY2FuX3VwZGF0ZV91c2VyIiwiY2FuX3JlYWRfdXNlciIsImNhbl9jcmVhdGVfdXNlciIsImNhbl9kZWxldGVfdXNlciJdLCJqdGkiOiJhYmQ4NjhmOC1hNGRmLTQ3NDMtOWY0NC04ZGMzYTQ1ZDFkMDQiLCJlbWFpbCI6IndpbGxpYW1AZ21haWwuY29tIiwiY2xpZW50X2lkIjoiVVNFUl9DTElFTlRfQVBQIn0.IL461k_MotXasbW3Fd2jwFITr2wz3FirD7WK6jtlMEYHMxg2CUpoJvdXtWh-E5wodn3dA8uQ6zw59LEFaZYNBx3s7FjBxYsFa6a53Kd2InW4IV23a_PzQTEgWMNferuys7eFbz26VOEqLZh0IwMBkRBOe2Bi6vX_yF9Ru3Xww-z9IfRbfOZnpeCxqhHwONP_AZnAIcRP_VO59szyGGpmNH7MQjOyqBuEoh-OHf0KiWO-6aih0YgXphjhIS7rKl9Rr21B9VmJJncUWtbMVvyw8_PsrAQ00TjLDBIg4cWRFHtYwMjuB2oTS9nEXZCGHxRVCqahtCwSKZW3d0F_lphH3A';
export const TOKEN_NAME: string = 'jwt_token';

@Injectable()
export class AuthService {

  private authurl: string = 'http://localhost:9001';
  private url: string = 'http://localhost:8081';
  private headers = new Headers({ 'Content-Type': 'multipart/form-data' });
  private authResponse : AuthToken;
  private httpEvent : HttpEvent<{}>;

  constructor(private http: HttpWrapperService) { }

  getToken(): string {

    return     localStorage.getItem(TOKEN_NAME);    ;
    //return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decod(token);
    localStorage.setItem('user_id', decoded.id);
    
    if (decoded.exp === undefined) return null;

    const date = new Date(0); 
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if(!token) token = this.getToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    console.log(date);

    if(date === undefined) return false;
    //return false;
    return !(date.valueOf() > new Date().valueOf()); 
  }

  /* login(user): Promise<string> {
    return this.http
      .post(`${this.url}/login`, JSON.stringify(user), { headers: this.headers })
      .toPromise()
      .then(res => res.text());
  } */

  validateUser(userName, password) : Observable<any>{
    console.log("this is test");
    const headers = new Headers({ 'Content-Type': 'multipart/form-data8' });
    const params = new HttpParams()
    .set('client_secret', 'fcskysecret')
    .set('username', userName)
    .set('password', password)
    .set('client_id', 'USER_CLIENT_APP')
    .set('grant_type', 'password');

    return this.http
      .postOptions(`${this.authurl}/oauth/token`, ``, { headers: this.headers,params,observe:'response'}).pipe(catchError(this.handleError));;
    
      console.log("jttp event is"+this.httpEvent);
      
  }
  private handleError(error: any): Observable<never> {
    return throwError(error || 'Server error');
  }

  ngOninit(){
    localStorage.setItem("auth_token",auth_token);
  }
}