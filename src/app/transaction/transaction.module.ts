import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AddTransasctionComponent } from './add-transasction/add-transasction.component';

@NgModule({
  declarations: [AddTransasctionComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[
    AddTransasctionComponent
  ]
})
export class TransactionModule { }
