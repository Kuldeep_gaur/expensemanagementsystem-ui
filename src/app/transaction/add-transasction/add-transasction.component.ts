import { Component, OnInit } from '@angular/core';
import{ NgForm} from '@angular/forms';
import { TransactionService } from '../../data-service/transaction.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-transasction',
  templateUrl: './add-transasction.component.html',
  styleUrls: ['./add-transasction.component.scss']
})
export class AddTransasctionComponent implements OnInit {
  public amount: number;
  public category: string;
  public isCredit: boolean;
  public description: string;
  public transaction : Transaction;
  public transactionResponse : Transaction;
  public categories : Array<{id: number, category: string}>;
  
  constructor(private _transactionService : TransactionService,private router: Router) { }

  ngOnInit() {
    this.amount = 0;
    this.category = '';
    this.isCredit=false;
    this.description = '';
    this._transactionService.getAllCategories().subscribe(
      data => this.categories=data
    );
  }

  resetForm(form: NgForm){ 
    console.log(form);
    form.reset();   
  }

  addTransaction(){
    console.log(this.amount);
    this.transaction = new Transaction();
    this.transaction.amount = this.amount;
    this.transaction.transaction_categories_id = this.category;
    this.transaction.is_credit = this.isCredit;
    this.transaction.description = this.description;
    this.transaction.createdOn = new Date();
    this.transaction.updatedOn = new Date();
    this.transaction.user_id = parseInt(localStorage.getItem("user_id"));
    console.log(this.transaction);
    this._transactionService.addTransaction(this.transaction).subscribe(
      data => this.route(data)
    );
  }

  route(transaction){
    this.transactionResponse = transaction;
    if(this.transactionResponse!=null){
      window.location.href = window.location.href;
    }
  }

}

export class Transaction {
  public user_id : number;
  public amount: number;
  public transaction_categories_id: string;
  public is_credit: boolean;
  public description: string;
  public createdOn: Date;
  public updatedOn: Date;
  constructor() { }  
}
