import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})


export class LoginComponent {
  constructor(private router: Router, private _authService : AuthService) { }

  userName: String;
  password: String;

  validateUser() {
    this._authService.validateUser(this.userName,this.password).subscribe((res:any)=>{
      if(res.ok){
        this._authService.setToken(res.body.access_token);
        this.navigateToDashboard();
      } else {
        this._authService.setToken("");
      }
    });
  }

  registerUser(){
    this.router.navigateByUrl("register");
  }

  navigateToDashboard(){
    this.router.navigateByUrl("ExpenseManagement/dashboard");
  }
}
