import { Component, OnInit } from '@angular/core';
import{ NgForm} from '@angular/forms';

@Component({
  selector: 'app-add-user-details',
  templateUrl: './addUserDetails.component.html'
})
export class AddUserDetailsComponent implements OnInit {

  public userName: String;
  public emailId: String;
  public mobileNum: String;
  public dateOfBirth: String;
  constructor(
  ) {  }

  ngOnInit() {
  }

  resetForm(form: NgForm){ 
    form.reset();   
  }

  addUserDetails(form: NgForm){
    form.reset();
  }
}
