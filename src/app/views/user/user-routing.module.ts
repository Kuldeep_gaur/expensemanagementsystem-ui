import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {  AddUserDetailsComponent } from './addUserDetails.component';

const routes: Routes = [
  {
    path: '',
    component: AddUserDetailsComponent,
    data: {
      title: 'AddUserDetails'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
