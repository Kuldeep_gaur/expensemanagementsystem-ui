import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AddUserDetailsComponent } from './addUserDetails.component';

// Buttons Routing
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ],
  declarations: [
    AddUserDetailsComponent
  ]
})
export class UserModule { }
