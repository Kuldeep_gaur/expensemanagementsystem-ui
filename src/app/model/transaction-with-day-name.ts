export class TransactionWithDayName {
    dayname: string;
    creditAmount: number;
    creditPercent: number;
    debitAmount: number;
    debitPercent: number;
    amount: number;
    constructor() { }  
  }
  