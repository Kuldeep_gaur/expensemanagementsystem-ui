
import { throwError as observableThrowError, Observable } from 'rxjs';

import { Injectable, Injector } from '@angular/core';
import {
    HttpInterceptor,
    HttpEvent,
    HttpResponse,
    HttpRequest,
    HttpHandler,
    HttpErrorResponse,
    HttpHeaders
} from '@angular/common/http';

import { map, catchError, finalize } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { $ } from 'protractor';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
    constructor(
        private injector: Injector
    ) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> { 
        let headers: HttpHeaders = req.headers;
        let authService = this.injector.get(AuthService);

        if(req.url.indexOf('auth') != -1){
            headers = headers.set('Authorization', 'Basic VVNFUl9DTElFTlRfQVBQOnBhc3N3b3Jk');
            headers = headers.set('Content-Type', 'application/json');
        } else if (
            req.url.indexOf('.json') == -1 &&
            req.url.indexOf('oauth') == -1 &&
            req.url.indexOf('jsonip') == -1 &&
            req.url.indexOf('forgotPassword') == -1
        ) {
                console.log("request url is "+req.url);

            headers = headers
            .set('Authorization', 'Bearer '+authService.getToken());

            if (req.url.indexOf('/downloadFile') != -1) {
                headers = headers.set('Content-Type', 'application/octet-stream');
            } else if (req.url.indexOf('/upload') == -1) {
                headers = headers.set('Content-Type', 'application/json');
                headers = headers.set('Accept', 'application/json');
            }
        }
        // custom headers
        const customReq = req.clone({
            headers: headers
        });

        return next.handle(customReq).pipe(
            map((event: HttpResponse<any>) => {
                /* if (event instanceof HttpResponse) {
                    console.log('custom response' + event);
                    this.appStorage.setString('Content-Type', event.headers.get('Content-Type'));
                } */

                return event;
            }),
            catchError(error => {
                if (error instanceof HttpErrorResponse) {
                    console.log('custom Error Response:      Error: ' + error.message);
                }
                /* if (Config.IS_WEB) {
                    const errorsHandler = this.injector.get(ErrorsHandler);
                    errorsHandler.handleError(error); 
                } */
                return observableThrowError(error);
            }),
            finalize(() => {})
        );
    }
}
